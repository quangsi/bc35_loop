document
  .getElementById("btn-show-result")
  .addEventListener("click", function () {
    var contentHTML = "";
    var number = document.getElementById("txt-number").value * 1;
    var count = 0;
    while (number > 1) {
      var content = `<p>  Count : ${count}  -  Number :  ${number} </p>`;
      contentHTML += content;
      count++;
      number = Math.floor(number / 2);
    }
    var resultEl = document.getElementById("result");

    resultEl.innerHTML = contentHTML;
    resultEl.classList.add("text-danger");
    resultEl.classList.add("bg-primary");
    // classList.remove
  });
