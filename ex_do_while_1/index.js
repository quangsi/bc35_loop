function inKetQua() {
  var number = document.getElementById("txt-number").value * 1;

  var result = 0;
  var step = 1;
  do {
    result += step;
    step++;
  } while (step <= number);
  console.log("result: ", result);
  document.getElementById("result").innerText = result;
}
